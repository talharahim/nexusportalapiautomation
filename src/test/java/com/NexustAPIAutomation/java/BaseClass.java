package com.NexustAPIAutomation.java;

import java.sql.SQLException;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

public class BaseClass {

	
	void BeforeTest() throws ClassNotFoundException, SQLException, InterruptedException {
		CommonMethods.CompanyDBRestore();
	}
}
